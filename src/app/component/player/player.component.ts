import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {initFlowbite} from "flowbite";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrl: './player.component.scss'
})
export class PlayerComponent implements OnInit{

  constructor(@Inject(PLATFORM_ID) private platformId: object) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      initFlowbite();
    }
  }
}
