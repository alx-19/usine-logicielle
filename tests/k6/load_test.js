import http from 'k6/http';
import { check, sleep } from 'k6';

// Définir les scénarios explicitement
const scenarios = {
    gradualRampUpAndDown: [
        { duration: '1m', target: 20 }, // Monte à 20 utilisateurs en 1 minute
        { duration: '3m', target: 20 }, // Maintient 20 utilisateurs pendant 3 minutes
        { duration: '1m', target: 0 },  // Descend à 0 utilisateurs en 1 minute
    ],
    rampUpAndDownQuickly: [
        { duration: '2m', target: 50 }, // Monte à 50 utilisateurs en 2 minutes
        { duration: '1m', target: 0 },  // Descend à 0 utilisateurs en 1 minute
    ],
    spikeTest: [
        { duration: '30s', target: 100 }, // Monte rapidement à 100 utilisateurs en 30 secondes
        { duration: '10s', target: 0 },   // Descend immédiatement à 0 utilisateurs
    ],
    constantLoad: [
        { duration: '5m', target: 30 },  // Maintient 30 utilisateurs pendant 5 minutes
    ]
};

// Choisir un scénario au hasard pour le test
const chosenScenario = scenarios.gradualRampUpAndDown;

export let options = {
    stages: chosenScenario,
};

const BASE_URL = __ENV.TEST_URL;

export default function () {
    let res = http.get(`${BASE_URL}/posts`); // Changez le chemin d'accès en fonction de votre URL de test
    check(res, { 'status was 200': (r) => r.status === 200 });
    sleep(1);
}
